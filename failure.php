<?php echo '<?xml version="1.1" encoding="utf-8"?>'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
	<head>
		<title>YouTube To Mp3 Converter</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" href="css/bootstrap.min.css" >
		<link rel="stylesheet" href="css/main.css" >
	</head>
	<body>
		<div class="navbar navbar-inverse navbar-fixed-top">
		  <div class="navbar-inner">
			<div class="container">
			  <a class="brand" href="index.php">YouTube To Mp3 Converter</a>
			</div>
		  </div>
		</div>
		<div class="container">
			<div class="well">
				<h3>Error</h3>
				<p>
					Error in generating MP3 file!
					<br/>
					<?php 
						session_start();
						echo @$_SESSION['error_message'];
					?>
				</p>
				<input type="button" class="btn btn-danger" value="Back to Main" onclick="window.location.href='index.php'" >
			</div>
		</div>
	</body>
</html>
